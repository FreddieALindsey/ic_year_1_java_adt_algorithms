# Java Algorithms for 1st year
A collection of algorithms taught for Java during Computing Year 1 at Imperial College

Where linked, the types have been done as a past paper and my interpretation of the answers given.

# Algorithms written:
- Trees
  - AVL
- [Heap](https://github.com/FreddieShoreditch/javaadthuffmanencoding_fl1414)

# Algorithms not written:
- Trees
  - Red-black
  - Binary Search
- Stacks
- Queues
  - Priority Queue
- Lists
  - [Linked Lists](https://github.com/FreddieShoreditch/javavideogameandtictactoe_fl1414)
